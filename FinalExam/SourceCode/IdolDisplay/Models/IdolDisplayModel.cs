using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
namespace IdolDisplay.Models
{
	public class IdolUser : IdentityUser{
		 public string FirstName { get; set;}
		 public string LastName { get; set;}
 }

	 public class Idols {
		public int	IdolsID { get; set;}
		 [DataType(DataType.Date)]
		public string  IdelDate { get; set; }
		public string  IdelDetail { get; set; }
		public string  Idelloca { get; set; }
		 public string Idelday { get; set; }
	}
}