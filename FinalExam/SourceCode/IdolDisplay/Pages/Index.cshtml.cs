﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore; 
using IdolDisplay.Data; 
using IdolDisplay.Models; 

namespace IdolDisplay.Pages
{
    public class IndexModel : PageModel
    {
        private IdolDisplay.Data.IdolDisplayContext _context; 
 
        public IndexModel(IdolDisplay.Data.IdolDisplayContext context)         {  
            _context = context;         
            }
            
        public IList<Idols> Idol { get;set; } 
 
        public async Task OnGetAsync() {             
            Idol = await _context.idolList.ToListAsync();     
        }         
    }
}
