﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore; 
using IdolDisplay.Data; 
using IdolDisplay.Models; 

namespace IdolDisplay.Pages
{
    public class AboutModel : PageModel
    {
        private IdolDisplay.Data.IdolDisplayContext _context; 
 
        public AboutModel(IdolDisplay.Data.IdolDisplayContext context)         {  
            _context = context;         
            }
            
        public IList<Idols> Idolza { get;set; } 
 
        public async Task OnGetAsync() {             
            Idolza = await _context.idolList.ToListAsync();     
        }         
    }
}
