using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolDisplay.Data;
using IdolDisplay.Models;

namespace IdolDisplay.Pages.Admin
{
    public class DetailsModel : PageModel
    {
        private readonly IdolDisplay.Data.IdolDisplayContext _context;

        public DetailsModel(IdolDisplay.Data.IdolDisplayContext context)
        {
            _context = context;
        }

        public Idols Idols { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Idols = await _context.idolList.FirstOrDefaultAsync(m => m.IdolsID == id);

            if (Idols == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
