using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolDisplay.Data;
using IdolDisplay.Models;

namespace IdolDisplay.Pages.Admin
{
    public class IndexModel : PageModel
    {
        private readonly IdolDisplay.Data.IdolDisplayContext _context;

        public IndexModel(IdolDisplay.Data.IdolDisplayContext context)
        {
            _context = context;
        }

        public IList<Idols> Idols { get;set; }

        public async Task OnGetAsync()
        {
            Idols = await _context.idolList.ToListAsync();
        }
    }
}
