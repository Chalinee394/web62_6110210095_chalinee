using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IdolDisplay.Data;
using IdolDisplay.Models;

namespace IdolDisplay.Pages.Admin
{
    public class EditModel : PageModel
    {
        private readonly IdolDisplay.Data.IdolDisplayContext _context;

        public EditModel(IdolDisplay.Data.IdolDisplayContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Idols Idols { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Idols = await _context.idolList.FirstOrDefaultAsync(m => m.IdolsID == id);

            if (Idols == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Idols).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IdolsExists(Idols.IdolsID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool IdolsExists(int id)
        {
            return _context.idolList.Any(e => e.IdolsID == id);
        }
    }
}
