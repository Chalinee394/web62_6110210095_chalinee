using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolDisplay.Data;
using IdolDisplay.Models;

namespace IdolDisplay.Pages.Admin
{
    public class DeleteModel : PageModel
    {
        private readonly IdolDisplay.Data.IdolDisplayContext _context;

        public DeleteModel(IdolDisplay.Data.IdolDisplayContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Idols Idols { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Idols = await _context.idolList.FirstOrDefaultAsync(m => m.IdolsID == id);

            if (Idols == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Idols = await _context.idolList.FindAsync(id);

            if (Idols != null)
            {
                _context.idolList.Remove(Idols);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
