using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using IdolDisplay.Data;
using IdolDisplay.Models;

namespace IdolDisplay.Pages.Admin
{
    public class CreateModel : PageModel
    {
        private readonly IdolDisplay.Data.IdolDisplayContext _context;

        public CreateModel(IdolDisplay.Data.IdolDisplayContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Idols Idols { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.idolList.Add(Idols);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}