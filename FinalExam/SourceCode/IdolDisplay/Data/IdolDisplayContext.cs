using IdolDisplay.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
namespace IdolDisplay.Data
{
 public class IdolDisplayContext : IdentityDbContext<IdolUser>
 {
	public DbSet<Idols> idolList { get; set; }
	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
	 optionsBuilder.UseSqlite(@"Data source=IdolDisplay.db");
	 }
 }
}

